package exercise;

import java.util.List;
import java.util.stream.Collectors;

// BEGIN
class App {

    public static List<String> buildAppartmentsList(List<Home> apartments, int apartmentsNumToOutput) {
        return apartments.stream()
                .sorted(Home::compareTo)
                .limit(apartmentsNumToOutput)
                .map(Object::toString)
                .collect(Collectors.toList());
    }
}
// END
