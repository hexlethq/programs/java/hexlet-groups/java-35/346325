package exercise;

// BEGIN
class ReversedSequence implements CharSequence {

    private String sequence;

    public ReversedSequence(String sequence) {
        this.sequence = reverse(sequence);
    }

    @Override
    public int length() {
        return sequence.length();
    }

    @Override
    public char charAt(int index) {
        return sequence.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return sequence.subSequence(start, end);
    }

    private String reverse(String sequence) {
        return new StringBuilder(sequence)
                .reverse()
                .toString();
    }

    @Override
    public String toString() {
        return sequence;
    }
}
// END
