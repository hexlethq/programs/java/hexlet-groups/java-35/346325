package exercise;
import exercise.connections.Connection;
import exercise.connections.Disconnected;

// BEGIN
public class TcpConnection {
    private Connection connection;
    private String address;
    private int port;

    public TcpConnection(String address, int port) {
        this.address = address;
        this.port = port;
        this.connection = new Disconnected(this);
    }

     public void connect() {
        connection.connect();
    }

    public void disconnect() {
        connection.disconnect();
    }

    public void write(String data) {
        connection.write(data);
    }

    public String getCurrentState() {
        return connection.getCurrentState();
    }

    public void setConnection(Connection state) {
        this.connection = state;
    }
}
// END
