package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String phrase) {
        String[] words = phrase.split(" ");
        String abbreviation = "";

        for (int i = 0; i < words.length; i++)
        {
            if (words[i].length() != 0)
            {
                abbreviation += Character.toUpperCase(words[i].charAt(0));
            }
        }
        return abbreviation;
    }
    // END
}
