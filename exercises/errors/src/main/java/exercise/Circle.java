package exercise;

// BEGIN
class Circle {
    private Point center;
    private int radius;

    public Circle(Point center, int radius) {
        this.center = center;
        this.radius = radius;
    }

    public int getRadius() {
        return this.radius;
    }

    public double getSquare() throws NegativeRadiusException {
        if (this.radius >= 0) {
            return Math.PI * radius * radius;
        } else {
            throw new NegativeRadiusException("");
        }
    }
}
// END
