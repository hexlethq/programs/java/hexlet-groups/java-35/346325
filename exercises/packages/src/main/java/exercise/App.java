// BEGIN
package exercise;

import exercise.geometry.Point;
import exercise.geometry.Segment;

class App {

    public static double[] getMidpointOfSegment(double[][] segment) {
        double midX = (segment[0][0] + segment[1][0]) / 2;
        double midY = (segment[0][1] + segment[1][1]) / 2;
        return new double[] {midX, midY};
    }

    public static double[][] reverse(double[][] segment) {
        return new double[][] {segment[1].clone(), segment[0].clone()};
    }

    public static boolean isBelongToOneQuadrant(double[][] segment) {
        if (getQuadrant(segment[0]) == 0 || getQuadrant(segment[1]) == 0) return false;
        return getQuadrant(segment[0]) == getQuadrant(segment[1]);
    }

    public static int getQuadrant(double[] point){
        if (Point.getX(point) > 0 && Point.getY(point) > 0) {
            return 1;
        } else if (Point.getX(point) < 0 && Point.getY(point) > 0) {
            return 2;
        } else if (Point.getX(point) < 0 && Point.getY(point) < 0) {
            return 3;
        } else if (Point.getX(point) > 0 && Point.getY(point) < 0) {
            return 4;
        }

        return 0;
    }

}
// END
