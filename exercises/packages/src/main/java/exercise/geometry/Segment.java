// BEGIN
package exercise.geometry;

public class Segment {

    public static double[][] makeSegment(double[] firstPoint, double[] secondPoint) {
        return new double[][] {firstPoint, secondPoint};
    }

    public static double[] getBeginPoint(double[][] segment) {
        return segment[0];
    }

    public static double[] getEndPoint(double[][] segment) {
        return segment[1];
    }
}
// END
