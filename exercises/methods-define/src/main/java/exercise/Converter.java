package exercise;

class Converter {
    // BEGIN
    public static int convert(int numberToConvert, String convertDirection){

        if (convertDirection.equals("b"))
        {
            return numberToConvert * 1024;
        } else if (convertDirection.equals("Kb"))
        {
            return numberToConvert / 1024;
        }
        return 0;
    }

    public static void main(String[] args) {
        System.out.println("10 Kb = " + convert(10,"b") + " b");
    }
    // END
}
