package exercise;

import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
class PairedTag extends Tag {
    private String body;
    private List<Tag> childs;

    public PairedTag(String name, Map<String, String> attributes, String body, List<Tag> childs) {
        super(name, attributes);
        this.body = body;
        this.childs = childs;
    }

    @Override
    public String toString() {
        String childsAsString = childs.stream()
                .map(Tag::toString)
                .collect(Collectors.joining());

        return super.toString() + body + childsAsString + "</" + getName() + ">";
    }
}
// END
