package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
class FileKV implements KeyValueStorage {

    String pathToStorage;

    public FileKV(String pathToStorage, Map<String, String> startValueOfStorage) {
        this.pathToStorage = pathToStorage;
        String storageAsString = Utils.serialize(startValueOfStorage);
        Utils.writeFile(pathToStorage, storageAsString);
    }

    @Override
    public void set(String key, String value) {
        Map<String, String> storage = Utils.unserialize(Utils.readFile(pathToStorage));
        storage.put(key, value);
        Utils.writeFile(pathToStorage, Utils.serialize(storage));
    }

    @Override
    public void unset(String key) {
        Map<String, String> storage = Utils.unserialize(Utils.readFile(pathToStorage));
        storage.remove(key);
        Utils.writeFile(pathToStorage, Utils.serialize(storage));
    }

    @Override
    public String get(String key, String defaultValue) {
        Map<String, String> storage = Utils.unserialize(Utils.readFile(pathToStorage));
        return storage.getOrDefault(key, defaultValue);
    }

    @Override
    public Map<String, String> toMap() {
        return Utils.unserialize(Utils.readFile(pathToStorage));
    }
}
// END
