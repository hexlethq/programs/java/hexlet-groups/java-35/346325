package exercise;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

// BEGIN
class App {

    public static void swapKeyValue(KeyValueStorage storage) {
        Map<String, String> storageMap = storage.toMap();

        for (String key : storageMap.keySet()) {
            String value = storage.get(key,"");
            storage.set(value, key);
            storage.unset(key);
        }
    }
}
// END
