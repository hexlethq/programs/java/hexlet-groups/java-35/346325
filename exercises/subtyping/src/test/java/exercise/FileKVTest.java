package exercise;

import java.util.HashMap;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
// BEGIN

// END


class FileKVTest {

    private static Path filepath = Paths.get("src/test/resources/file").toAbsolutePath().normalize();

    @BeforeEach
    public void beforeEach() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String content = mapper.writeValueAsString(new HashMap<String, String>());
        Files.writeString(filepath, content, StandardOpenOption.CREATE);
    }

    // BEGIN
    @Test
    void fileKVTest() {
        String pathToStorage = "src/test/resources/file";
        FileKV fileKV = new FileKV(pathToStorage, Map.of("key", "value"));

        Map<String, String> actual = Utils.unserialize(Utils.readFile(pathToStorage));
        Assertions.assertEquals(Map.of("key", "value"), actual);

        Assertions.assertEquals("value", fileKV.get("key", "default"));
        Assertions.assertEquals("default", fileKV.get("k", "default"));

        fileKV.set("key2", "value2");
        actual = Utils.unserialize(Utils.readFile(pathToStorage));
        Assertions.assertEquals(Map.of("key", "value", "key2", "value2"), actual);

        fileKV.unset("key");
        actual = Utils.unserialize(Utils.readFile(pathToStorage));
        Assertions.assertEquals(Map.of("key2", "value2"), actual);

        Assertions.assertEquals(Map.of("key2", "value2"), fileKV.toMap());
    }
    // END
}
