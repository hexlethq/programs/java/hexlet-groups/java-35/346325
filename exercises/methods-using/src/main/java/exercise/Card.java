package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String hiddenCard = String.format("%" + starsCount + "s", "*").replace(" ", "*")
                + cardNumber.substring(cardNumber.length() - 4);

        System.out.println(hiddenCard);
        // END
    }
}
