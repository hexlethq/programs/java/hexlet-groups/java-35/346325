package exercise.controller;

import exercise.model.City;
import exercise.repository.CityRepository;
import exercise.service.WeatherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;


@RestController
public class CityController {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private WeatherService weatherService;

    // BEGIN
    @GetMapping(path = "/cities/{id}")
    public Map getFullInfoAboutWeather(@PathVariable long id) {
        return weatherService.getWeatherIn(id);
    }

    @GetMapping(path = "/search")
    public List<Map> getTemperature(@RequestParam(required = false, defaultValue = "") String name) {
        List<City> cities = name.isEmpty()
                ? cityRepository.findAllByOrderByName()
                : cityRepository.findByNameIgnoreCaseStartingWith(name);

        return cities.stream()
                .map(city -> weatherService.getWeatherIn(city.getId()))
                .map(e -> Map.of("name", e.get("name"), "temperature", e.get("temperature")))
                .collect(Collectors.toList());
    }
    // END
}

