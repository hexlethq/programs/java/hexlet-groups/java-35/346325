package exercise;

import java.util.ArrayList;
import java.util.Arrays;


// BEGIN
class Kennel {
    private static int puppyCount = 0;
    private static ArrayList<String[]> allPuppies = new ArrayList<>();

    public static void addPuppy(String[] puppy) {
        allPuppies.add(puppy);
        puppyCount++;
    }

    public static void addSomePuppies(String[][] puppies) {
        for (String puppy[] : puppies){
            allPuppies.add(puppy);
            puppyCount++;
        }
    }

    public static int getPuppyCount() {
        return puppyCount;
    }

    public static boolean isContainPuppy(String name) {
        for (int i = 0; i < allPuppies.size(); i++)
        {
            if (allPuppies.get(i)[0].equals(name))
            {
                return true;
            }
        }

        return false;
    }

    public static String[][] getAllPuppies() {
        String[][] puppies = new String[allPuppies.size()][2];

        for (int i = 0; i < puppies.length; i++)
        {
            puppies[i] = allPuppies.get(i);
        }

        return puppies;
    }

    public static String[] getNamesByBreed(String breed) {
        ArrayList<String> names = new ArrayList<>();

        for (String[] puppy : allPuppies)
        {
            if (puppy[1].equals(breed))
            {
                names.add(puppy[0]);
            }
        }

        return names.toArray(new String[names.size()]);
    }

    public static void resetKennel() {
        puppyCount = 0;
        allPuppies.clear();
    }

    public static boolean removePuppy(String name) {
        for(int i = 0; i < allPuppies.size(); i++)
        {
            if (allPuppies.get(i)[0].equals(name))
            {
                puppyCount--;
                allPuppies.remove(i);
                return true;
            }
        }
        return false;
    }

}
// END
