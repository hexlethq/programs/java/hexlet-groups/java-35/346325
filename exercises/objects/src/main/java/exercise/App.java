package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Locale;

class App {
    // BEGIN
    public static String buildList(String[] words){

        StringBuilder bulletedList = new StringBuilder();

        for (String word : words)
        {
            bulletedList.append("  <li>" + word + "</li>\n");
        }

        if (bulletedList.length() != 0)
        {
            bulletedList.insert(0, "<ul>\n");
            bulletedList.append("</ul>");
        }

        return bulletedList.toString();
    }

    public static String getUsersByYear(String[][] users, int year) {
        StringBuilder usersByYear = new StringBuilder();

        for (int i = 0; i < users.length; i++)
        {
            LocalDate dateYear = LocalDate.parse(users[i][1]);

            if (dateYear.getYear() == year)
            {
                usersByYear.append("  <li>" + users[i][0] + "</li>\n");
            }
        }

        if (usersByYear.length() != 0)
        {
            usersByYear.insert(0,"<ul>\n");
            usersByYear.append("</ul>");
        }

        return usersByYear.toString();
    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy", Locale.ENGLISH);
        LocalDate beforeDate = LocalDate.parse(date, formatter);
        LocalDate youngestUser = LocalDate.MIN;
        String youngestUserName = "";

        for (int i = 0; i < users.length; i ++)
        {
            LocalDate birthDate =  LocalDate.parse(users[i][1]);
            if (birthDate.isBefore(beforeDate) && birthDate.isAfter(youngestUser))
            {
                youngestUser = birthDate;
                youngestUserName = users[i][0];
            }
        }
        return youngestUserName;
        // END
    }
}
