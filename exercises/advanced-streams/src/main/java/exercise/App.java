package exercise;

import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static String getForwardedVariables(String content) {
        return Stream.of(content)
                .map(str -> str.split("\n"))
                .flatMap(Arrays::stream)
                .filter(x -> x.startsWith("environment"))
                .map(x -> x.replaceAll("environment=|\"|\s", ""))
                .map(x -> x.split(","))
                .flatMap(Arrays::stream)
                .filter(x -> x.startsWith("X_FORWARDED_"))
                .map(x -> x.replaceAll("X_FORWARDED_", ""))
                .collect(Collectors.joining(","));
    }
}
//END
