package exercise;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] array) {
        int indexOfMaxNegative = -1;
        int maxNegative = Integer.MIN_VALUE;

        for (int i = 0; i < array.length; i++)
        {
            if (maxNegative < array[i] && array[i] < 0)
            {
                maxNegative = array[i];
                indexOfMaxNegative = i;
            }
        }

        return indexOfMaxNegative;
    }

    public static int[] getElementsLessAverage(int[] array) {
        double average = getAverage(array);
        int countOfLessAverage = 0;

        for (int i : array)
        {
            if (i < average) countOfLessAverage++;
        }

        int[] elementsLessAverage = new int[countOfLessAverage];

        int index = 0;
        for (int i : array)
        {
            if (i < average)
            {
                elementsLessAverage[index] = i;
                index++;
            }
        }
        return elementsLessAverage;
    }

    static double getAverage(int[] array) {
        double sum = 0;

        for (int i : array)
        {
            sum += i;
        }
        return sum / array.length;
    }
    // END
}
