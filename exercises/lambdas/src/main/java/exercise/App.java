package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static String[][] enlargeArrayImage(String[][] image) {

        String[][] enlargedImage = Arrays.stream(image)
                .map(x -> Arrays.stream(x)
                        .flatMap(c -> Stream.of(c,c))
                        .toArray(String[]::new))
                .flatMap(x -> Stream.of(x, x))
                .toArray(String[][]::new);
        return enlargedImage;
    }
}
// END
