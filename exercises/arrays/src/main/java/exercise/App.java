package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] array) {
        int[] reversedArray = new int[array.length];

        for (int i = 0; i < array.length; i++)
        {
            reversedArray[i] = array[array.length - 1 - i];
        }

        return reversedArray;
    }

    public static int mult(int[] array) {
        int result = 1;

        for (int i : array)
        {
            result *= i;
        }

        return result;
    }
    // END
}
