package exercise.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletContext;

import java.sql.*;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import org.apache.commons.lang3.ArrayUtils;

import exercise.TemplateEngineUtil;


public class ArticlesServlet extends HttpServlet {

    private String getId(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return null;
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 1, null);
    }

    private String getAction(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return "list";
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 2, getId(request));
    }

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        String action = getAction(request);

        switch (action) {
            case "list":
                showArticles(request, response);
                break;
            default:
                showArticle(request, response);
                break;
        }
    }

    private void showArticles(HttpServletRequest request,
                          HttpServletResponse response)
                throws IOException, ServletException {

        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");
        // BEGIN
        List<Map<String, String>> articles = new ArrayList<>();
        final int articlesOnPage = 10;
        long maxPageNumber = 0;
        int pageNumber = request.getParameter("page") == null ? 1 :
                Integer.parseInt(request.getParameter("page"));
        String query = "SELECT * FROM articles ORDER BY id LIMIT ? OFFSET ?";
        try {
            PreparedStatement getArticlesFromDB = connection.prepareStatement(query);
            getArticlesFromDB.setInt(1, articlesOnPage);
            getArticlesFromDB.setInt(2, articlesOnPage * (pageNumber - 1));

            ResultSet rs = getArticlesFromDB.executeQuery();

            while (rs.next()) {
                articles.add(Map.of(
                        "id", rs.getString("id"),
                        "title", rs.getString("title")
                        )
                );
            }

            Statement getAmountOfArticles = connection.createStatement();
            ResultSet amountOfArticles = getAmountOfArticles.executeQuery("SELECT COUNT(*) FROM articles");
            amountOfArticles.next();
            long articlesAmount = amountOfArticles.getLong(1);
            maxPageNumber = (long) Math.ceil(articlesAmount/(double)articlesOnPage);

        } catch (SQLException e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        }

        request.setAttribute("articles", articles);
        request.setAttribute("page", pageNumber);
        request.setAttribute("maxPageNumber", maxPageNumber);
        // END
        TemplateEngineUtil.render("articles/index.html", request, response);
    }

    private void showArticle(HttpServletRequest request,
                         HttpServletResponse response)
                 throws IOException, ServletException {

        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");
        // BEGIN
        Map<String, String> article = new HashMap<>();
        String id = request.getPathInfo().substring(1);
        String query = "SELECT * FROM articles WHERE id = ?";
        try {
            PreparedStatement getArticleFromDB = connection.prepareStatement(query);
            getArticleFromDB.setString(1, id);

            ResultSet rs = getArticleFromDB.executeQuery();

            rs.next();

            if (rs.getRow() == 0) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }

            article.put("title", rs.getString("title"));
            article.put("body", rs.getString("body"));
        } catch (SQLException e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        }
        // END
        request.setAttribute("article", article);
        TemplateEngineUtil.render("articles/show.html", request, response);
    }
}
