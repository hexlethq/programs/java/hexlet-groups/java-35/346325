package exercise;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.Value;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

// BEGIN
@Value
// END
class Car {
    int id;
    String brand;
    String model;
    String color;
    User owner;

    // BEGIN
    public String serialize() {
        ObjectMapper objectMapper = new ObjectMapper();
        String CarInJSON = null;

        try {
            CarInJSON = objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return CarInJSON;
    }

    public static Car unserialize(String JSON) {
        ObjectMapper objectMapper = new ObjectMapper();
        Car car = null;
        try {
           car = objectMapper.readValue(JSON, Car.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return car;
    }
    // END
}
