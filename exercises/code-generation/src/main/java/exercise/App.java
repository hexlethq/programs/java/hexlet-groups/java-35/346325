package exercise;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Files;

// BEGIN
class App {

    public static void save(Path pathToFile, Car car) {
        try {
            Files.writeString(pathToFile, car.serialize());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Car extract(Path pathToJSONFile) {
        String JSONString = null;
        try {
            JSONString = Files.readString(pathToJSONFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Car.unserialize(JSONString);
    }
}
// END
