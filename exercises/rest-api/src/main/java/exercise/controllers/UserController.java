package exercise.controllers;

import io.javalin.http.Context;
import io.javalin.apibuilder.CrudHandler;
import io.ebean.DB;
import java.util.List;

import exercise.domain.User;
import exercise.domain.query.QUser;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.lang3.StringUtils;

public class UserController implements CrudHandler {

    public void getAll(Context ctx) {
        // BEGIN
        List<User> users = new QUser()
                .orderBy()
                .id.asc()
                .findList();

        String json = DB.json().toJson(users);

        ctx.json(json);
        // END
    };

    public void getOne(Context ctx, String id) {
        // BEGIN
        User user = new QUser()
                .id.eq(Long.parseLong(id))
                .findOne();

        String json = DB.json().toJson(user);

        ctx.json(json);
        // END
    };

    public void create(Context ctx) {
        // BEGIN
        User user = ctx.bodyValidator(User.class)
                .check(usr -> StringUtils.isNoneBlank(usr.getFirstName()), "Имя не должно быть пустым.")
                .check(usr -> StringUtils.isNoneBlank(usr.getLastName()), "Фамилия не должна быть пустой.")
                .check(usr -> EmailValidator.getInstance().isValid(usr.getEmail()), "Введён некорректный email.")
                .check(usr -> usr.getPassword().length() >= 4, "Пароль должен должен содержать не менее 4-х символов.")
                .check(usr -> StringUtils.isNumeric(usr.getPassword()), "Пароль должен состоять только из цифр.")
                .get();

        user.save();
        // END
    };

    public void update(Context ctx, String id) {
        // BEGIN
        String body = ctx.body();

        User user = DB.json().toBean(User.class, body);

        user.setId(id);
        user.update();
        // END
    };

    public void delete(Context ctx, String id) {
        // BEGIN
        new QUser()
                .id.equalTo(Long.parseLong(id))
                .delete();
        // END
    };
}
