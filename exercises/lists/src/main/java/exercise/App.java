package exercise;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

// BEGIN
public class App {

    public static boolean scrabble(String characterSet, String word) {
        if (characterSet.length() < word.length()) {
            return false;
        }

        List<Character> characters = new ArrayList<>();
        for (Character c : characterSet.toCharArray()) {
            characters.add(c);
        }

        word = word.toLowerCase();
        for (Character c : word.toCharArray()) {
            if (!characters.remove(c)) {
                return false;
            }
        }

        return true;
    }
}
//END
