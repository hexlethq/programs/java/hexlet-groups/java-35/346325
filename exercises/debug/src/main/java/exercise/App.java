package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int firstSegment, int secondSegment, int thirdSegment) {
        if (firstSegment + secondSegment > thirdSegment
            && secondSegment + thirdSegment > firstSegment
            && thirdSegment + firstSegment > secondSegment)
        {
            if (firstSegment == secondSegment && secondSegment == thirdSegment)
            {
                return "Равносторонний";
            } else if (firstSegment == secondSegment
                       || secondSegment == thirdSegment
                       || thirdSegment == firstSegment)
            {
                return "Равнобедренный";
            } else
            {
                return "Разносторонний";
            }
        }
        return "Треугольник не существует";
    }
    // END
}
