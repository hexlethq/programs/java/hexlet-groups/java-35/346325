package exercise;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// BEGIN
class Validator {

    public static List<String> validate(Address address) {
        List<String> nullFields = new ArrayList<>();
        for(Field field : address.getClass().getDeclaredFields()) {
            NotNull notNull = field.getAnnotation(NotNull.class);
            try {
                field.setAccessible(true);
                if (notNull != null && field.get(address) == null) {
                    nullFields.add(field.getName());
                }
            } catch (IllegalAccessException e) {
                e.getMessage();
            }
        }

        return nullFields;
    }

    public static Map<String, List<String>> advancedValidate(Address address) {
        Map<String, List<String>> invalidFields = new HashMap<>();
        for(Field field : address.getClass().getDeclaredFields()) {
            NotNull notNull = field.getAnnotation(NotNull.class);
            MinLength minLength = field.getAnnotation(MinLength.class);
            List<String> annotationErrors = new ArrayList<>();
            try {
                field.setAccessible(true);
                if (notNull != null && field.get(address) == null) {
                    annotationErrors.add("can not be null");
                }

                if (minLength != null && field.get(address) != null
                        && field.get(address).toString().length() < minLength.minLength()) {
                    annotationErrors.add("length less than " + minLength.minLength());
                }
            } catch (IllegalAccessException e) {
                e.getMessage();
            }
            if (annotationErrors.size() > 0) {
                invalidFields.put(field.getName(), annotationErrors);
            }
        }
        return invalidFields;
    }
}
// END
