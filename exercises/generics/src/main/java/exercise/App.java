package exercise;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Map.Entry;

// BEGIN
public class App {
    public static List<Map<String, String>> findWhere(List<Map<String, String>> books, Map<String, String> where) {
        List<Map<String, String>> foundBooks = new ArrayList<>();

        for (Map<String, String> book : books) {
            boolean isValuesMatching = true;
            for (String s : where.keySet()) {
                if (!where.get(s).equals(book.get(s))) {
                    isValuesMatching = false;
                }
            }
            if (isValuesMatching) {
                foundBooks.add(book);
            }
        }
        return foundBooks;
    }
}
//END
