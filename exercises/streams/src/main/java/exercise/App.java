package exercise;

import java.util.List;
import java.util.Arrays;

// BEGIN
public class App {
    public static long getCountOfFreeEmails(List<String> emails) {
        long freeEmails = 0;

        freeEmails = emails.stream()
                .filter(s -> isFreeEmail(s))
                .count();
        return freeEmails;
    }

    public static boolean isFreeEmail(String email) {
        return email.endsWith("@yandex.ru") || email.endsWith("@gmail.com") || email.endsWith("@hotmail.com");
    }
}
// END
