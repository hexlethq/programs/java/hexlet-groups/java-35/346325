package exercise;

class App {
    // BEGIN
    public static int[] sort(int[] array) {
        int[] sorted = array.clone();
        boolean isSorted = false;

        while (!isSorted)
        {
            isSorted = true;
            for (int i = 0; i < sorted.length - 1; i++)
            {
                if (sorted[i] > sorted[i+1])
                {
                    sorted[i] = sorted[i] + sorted[i + 1];
                    sorted[i + 1] = sorted[i] - sorted[i + 1];
                    sorted[i] = sorted[i] - sorted[i + 1];
                    isSorted = false;
                }
            }
        }
        return sorted;
    }
    // END
}
