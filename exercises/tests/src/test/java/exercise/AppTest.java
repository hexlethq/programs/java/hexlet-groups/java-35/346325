package exercise;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;

class AppTest {

    @Test
    void testTake() {
        // BEGIN
        List<Integer> list = List.of(1, 2, 3, 4, 5, 6);
        List<Integer> emptyList = new ArrayList<>();
        List<Integer> expected = List.of(1, 2, 3);

        assertThat(App.take(list, 3)).isEqualTo(expected);
        assertThat(App.take(list, 10)).isEqualTo(list);
        assertThat(App.take(emptyList, 3)).isEqualTo(emptyList);
        // END
    }
}
