package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class App {
    public static Map<String, Integer> getWordCount(String sentence) {
        String[] words = sentence.split(" ");

        Map<String, Integer> wordCount = new HashMap<>();

        if (sentence.length() == 0) {
            return wordCount;
        }

        for (String word : words) {
            wordCount.put(word, getNumOfOccurrences(word, words));
        }

        return wordCount;
    }

    public static int getNumOfOccurrences(String word, String[] words) {
        int wordCount = 0;

        for (String w : words) {
            if (word.equals(w)) {
                wordCount++;
            }
        }

        return wordCount;
    }

    public static String toString(Map<String, Integer> wordCount) {
        if (wordCount.isEmpty()) {
            return "{}";
        }

        String result = "{\n";

        for (Map.Entry<String, Integer> word : wordCount.entrySet()) {
            result += "  " + word.getKey() + ": " + word.getValue() + "\n";
        }

        return result + "}";
    }
}
//END
